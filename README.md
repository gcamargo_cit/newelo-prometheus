newelo-prometheus
========

Containers Docker para [Prometheus](https://prometheus.io/), [Grafana](http://grafana.org/), e [AlertManager](https://github.com/prometheus/alertmanager).

## Instalação

Clone este repositório, entre no diretório newelo-prometheus, e rode o compose up passando os parâmetros (utilize o -d para detached):

```bash
ADMIN_USER=admin ADMIN_PASSWORD=admin docker-compose up
```

Containers:

* Prometheus `http://<host>:9090`
* AlertManager `http://<host>:9093`
* Grafana `http://<host>:3000`
